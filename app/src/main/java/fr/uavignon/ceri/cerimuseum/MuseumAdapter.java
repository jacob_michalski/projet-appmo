package fr.uavignon.ceri.cerimuseum;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.uavignon.ceri.cerimuseum.data.Item;
import fr.uavignon.ceri.cerimuseum.data.MuseumResult;

class MuseumAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.cerimuseum.MuseumAdapter.ViewHolder> {

    private List<Item> itemsList;

    public void setItemsList(MuseumResult items) {
        itemsList = items.items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int listPosition) {
        viewHolder.itemName.setText(itemsList.get(listPosition).getName());
        viewHolder.itemDescription.setText(itemsList.get(listPosition).getDescription());
    }

    @Override
    public int getItemCount() {
        return itemsList == null ? 0 : itemsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName;
        TextView itemDescription;

        ViewHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.item_name);
            itemDescription = itemView.findViewById(R.id.item_description);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    /*long position = MuseumAdapter.this.itemsList.get(getAdapterPosition()).getId();
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setBookNum(position);
                    Navigation.findNavController(v).navigate(action);*/
                }
            });
        }
    }
}




