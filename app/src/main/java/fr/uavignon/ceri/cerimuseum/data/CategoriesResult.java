package fr.uavignon.ceri.cerimuseum.data;

import java.util.List;

public class CategoriesResult {
    public final boolean isLoading;
    public final List<String> categories;
    public final Throwable error;

    CategoriesResult(boolean isLoading, List<String> categories, Throwable error) {
        this.isLoading = isLoading;
        this.categories = categories;
        this.error = error;
    }
}
