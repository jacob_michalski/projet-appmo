package fr.uavignon.ceri.cerimuseum.data;

public class CollectionVersionResult {
    public final boolean isLoading;
    public final Float version;
    public final Throwable error;

    public CollectionVersionResult(boolean isLoading, Float version, Throwable error) {
        this.isLoading = isLoading;
        this.version = version;
        this.error = error;
    }
}
