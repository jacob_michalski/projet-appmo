package fr.uavignon.ceri.cerimuseum.data;

import java.util.List;

public class MuseumResult {
    public final boolean isLoading;
    public final List<Item> items;
    public final Throwable error;

    MuseumResult(boolean isLoading, List<Item> items, Throwable error) {
        this.isLoading = isLoading;
        this.items = items;
        this.error = error;
    }



}
