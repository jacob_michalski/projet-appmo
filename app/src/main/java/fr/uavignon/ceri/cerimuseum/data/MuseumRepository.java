package fr.uavignon.ceri.cerimuseum.data;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class MuseumRepository {
    private static volatile MuseumRepository INSTANCE;
    private final CMInterface api;

    public synchronized static MuseumRepository get() {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository();
        }

        return INSTANCE;
    }

    private MuseumRepository() {
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(CMInterface.class);
    }

    public LiveData<CollectionVersionResult> loadCollectionVersion() {
        final MutableLiveData<CollectionVersionResult> result = new MutableLiveData<>();

        result.setValue(new CollectionVersionResult(true, null, null));

        api.getCollectionVersion().enqueue(
                new Callback<Float>() {
                    @Override
                    public void onResponse(Call<Float> call,
                                           Response<Float> response) {
                        result.setValue(new CollectionVersionResult(false, response.body(), null));
                    }

                    @Override
                    public void onFailure(Call<Float> call, Throwable t) {
                        result.setValue(new CollectionVersionResult(false, null, t));
                    }
                });

        return result;
    }

    public LiveData<CollectionResult> loadCollection() {
        final MutableLiveData<CollectionResult> result = new MutableLiveData<>();

        result.setValue(new CollectionResult(true, null, null));

        api.getCollection().enqueue(
                new Callback<Map<String, Item>>() {
                    @Override
                    public void onResponse(Call<Map<String, Item>> call,
                                           Response<Map<String, Item>> response) {
                        result.setValue(new CollectionResult(false, response.body(), null));
                    }

                    @Override
                    public void onFailure(Call<Map<String, Item>> call, Throwable t) {
                        result.setValue(new CollectionResult(false, null, t));
                        for (Item item : result.getValue().collection.values()) {
                            Log.d("CREATION", item.id);
                        }
                    }
                });

        return result;
    }

    public LiveData<CategoriesResult> loadCategories() {
        final MutableLiveData<CategoriesResult> result = new MutableLiveData<>();

        result.setValue(new CategoriesResult(true, null, null));

        api.getCategories().enqueue(
                new Callback<List<String>>() {
                    @Override
                    public void onResponse(Call<List<String>> call,
                                           Response<List<String>> response) {
                        result.setValue(new CategoriesResult(false, response.body(), null));

                    }

                    @Override
                    public void onFailure(Call<List<String>> call, Throwable t) {
                        result.setValue(new CategoriesResult(false, null, t));
                    }
                });

        return result;
    }

    public LiveData<MuseumResult> loadMuseum() {
        final MutableLiveData<MuseumResult> result = new MutableLiveData<>();

        result.setValue(new MuseumResult(true, null, null));

        api.getCollection().enqueue(
                new Callback<Map<String, Item>>() {
                    @Override
                    public void onResponse(Call<Map<String, Item>> call,
                                           Response<Map<String, Item>> response) {
                        List<Item> elements = new ArrayList<>();
                        elements.addAll(response.body().values());
                        result.setValue(new MuseumResult(false, elements, null));
                    }

                    @Override
                    public void onFailure(Call<Map<String, Item>> call, Throwable t) {
                        result.setValue(new MuseumResult(false, null, t));
                    }
                });

        return result;

    }

}
