package fr.uavignon.ceri.cerimuseum.data;

import java.util.Map;

public class CollectionResult {
    public final boolean isLoading;
    public final Map<String, Item> collection;
    public final Throwable error;

    CollectionResult(boolean isLoading, Map<String, Item> collection, Throwable error) {
        this.isLoading = isLoading;
        this.collection = collection;
        this.error = error;
    }
}
