package fr.uavignon.ceri.cerimuseum.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Item {
    public String id = "";
    public String name = "";
    public List<String> categories = new ArrayList<>();
    public String description = "";
    public List<Integer> timeFrame = new ArrayList<>();
    public int year = 0;
    public String brand = "";
    public List<String> technicalDetails = new ArrayList<>();
    public Map<String, String> pictures = new HashMap<>();
    public boolean working;

    public Item() {}
    public Item(String name, List<String> categories, String description, List<Integer> timeFrame) {
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getCategories() {
        return categories;
    }

    public List<Integer> getTimeFrame() {
        return timeFrame;
    }

    public int getYear() {
        return year;
    }

    public String getBrand() {
        return brand;
    }

    public List<String> getTechnicalDetails() {
        return technicalDetails;
    }

    public Map<String, String> getPictures() {
        return pictures;
    }

    public boolean isWorking() {
        return working;
    }

}
