package fr.uavignon.ceri.cerimuseum.data;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface CMInterface {
    @GET("collectionversion")
    Call<Float> getCollectionVersion();
    @GET("collection")
    Call<Map<String, Item>> getCollection();
    @GET("categories")
    Call<List<String>> getCategories();
}
