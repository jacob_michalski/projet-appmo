package fr.uavignon.ceri.cerimuseum;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.cerimuseum.data.Item;
import fr.uavignon.ceri.cerimuseum.data.MuseumRepository;
import fr.uavignon.ceri.cerimuseum.data.CollectionResult;
import fr.uavignon.ceri.cerimuseum.data.CollectionVersionResult;
import fr.uavignon.ceri.cerimuseum.data.CategoriesResult;
import fr.uavignon.ceri.cerimuseum.data.MuseumResult;

public class ListViewModel extends AndroidViewModel {

    final private MuseumRepository repo = MuseumRepository.get();
    private LiveData<CollectionVersionResult> lastCollectionVersion;
    private LiveData<CollectionResult> lastCollection;
    private LiveData<CategoriesResult> lastCategories;
    private LiveData<MuseumResult> lastMuseum;
    private MutableLiveData<List<Item>> lastItemList;


    public ListViewModel(@NonNull Application application) {
        super(application);
    }

    void loadCollectionVersion() {
        lastCollectionVersion = repo.loadCollectionVersion();
    }

    void loadCollection() {
        lastCollection = repo.loadCollection();
    }

    void loadCategories() {
        lastCategories = repo.loadCategories();
    }

    void loadMuseum() {
        lastMuseum = repo.loadMuseum();
    }

    void load() {
        loadCollectionVersion();
        loadCollection();
        loadCategories();
        loadMuseum();
    }

    LiveData<MuseumResult> getMuseum() {
        if (lastCollectionVersion != null) {
            float actual = lastCollectionVersion.getValue().version;
            loadCollectionVersion();
            if (lastCollectionVersion.getValue().version > actual) {
                loadCollection();
                loadCategories();
                loadMuseum();
            }
        }
        else {
            load();
        }
        return lastMuseum;
    }

}

