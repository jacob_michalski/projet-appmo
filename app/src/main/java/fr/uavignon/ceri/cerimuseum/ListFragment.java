package fr.uavignon.ceri.cerimuseum;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

public class ListFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ProgressBar progress;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ListViewModel viewModel = new ViewModelProvider(this).get(ListViewModel.class);
        final MuseumAdapter adapter = new MuseumAdapter();

        recyclerView = view.findViewById(R.id.items);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        progress = view.findViewById(R.id.progress);

        viewModel.getMuseum().observe(getViewLifecycleOwner(), state -> {
            if (!state.isLoading && state.error == null) {
                progress.setVisibility(View.GONE);
                adapter.setItemsList(state);
            }
            else if (state.error != null) {
                progress.setVisibility(View.GONE);
                Snackbar.make(view, state.error.getMessage(),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Log.e("Museum", "Exception loading data", state.error);
            }
        });
    }
}